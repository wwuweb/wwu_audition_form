<?php
/**
 * @file
 * wwu_audition_form_pieces.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wwu_audition_form_pieces_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'available_audition_times';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Available Audition Times';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Available Audition Times';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_date_time' => 'field_date_time',
    'field_instrument' => 'field_instrument',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_date_time' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_instrument' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no available audition times for your selected instrument type.';
  $handler->display->display_options['empty']['area']['format'] = 'clean_html';
  /* Field: Content: Date and Time */
  $handler->display->display_options['fields']['field_timestamp']['id'] = 'field_timestamp';
  $handler->display->display_options['fields']['field_timestamp']['table'] = 'field_data_field_timestamp';
  $handler->display->display_options['fields']['field_timestamp']['field'] = 'field_timestamp';
  $handler->display->display_options['fields']['field_timestamp']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Instrument */
  $handler->display->display_options['fields']['field_instrument']['id'] = 'field_instrument';
  $handler->display->display_options['fields']['field_instrument']['table'] = 'field_data_field_instrument';
  $handler->display->display_options['fields']['field_instrument']['field'] = 'field_instrument';
  $handler->display->display_options['fields']['field_instrument']['delta_offset'] = '0';
  /* Sort criterion: Content: Date and Time (field_timestamp) */
  $handler->display->display_options['sorts']['field_timestamp_value']['id'] = 'field_timestamp_value';
  $handler->display->display_options['sorts']['field_timestamp_value']['table'] = 'field_data_field_timestamp';
  $handler->display->display_options['sorts']['field_timestamp_value']['field'] = 'field_timestamp_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'audition_timeslot' => 'audition_timeslot',
  );
  /* Filter criterion: Content: Free (field_free) */
  $handler->display->display_options['filters']['field_free_value']['id'] = 'field_free_value';
  $handler->display->display_options['filters']['field_free_value']['table'] = 'field_data_field_free';
  $handler->display->display_options['filters']['field_free_value']['field'] = 'field_free_value';
  $handler->display->display_options['filters']['field_free_value']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Content: Instrument (field_instrument) */
  $handler->display->display_options['filters']['field_instrument_value']['id'] = 'field_instrument_value';
  $handler->display->display_options['filters']['field_instrument_value']['table'] = 'field_data_field_instrument';
  $handler->display->display_options['filters']['field_instrument_value']['field'] = 'field_instrument_value';
  $handler->display->display_options['filters']['field_instrument_value']['value'] = array(
    'woodwind' => 'woodwind',
  );
  $handler->display->display_options['filters']['field_instrument_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_instrument_value']['expose']['operator_id'] = 'field_instrument_value_op';
  $handler->display->display_options['filters']['field_instrument_value']['expose']['label'] = 'Instrument';
  $handler->display->display_options['filters']['field_instrument_value']['expose']['operator'] = 'field_instrument_value_op';
  $handler->display->display_options['filters']['field_instrument_value']['expose']['identifier'] = 'field_instrument_value';
  $handler->display->display_options['filters']['field_instrument_value']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['field_instrument_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'audition-times';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['available_audition_times'] = $view;

  return $export;
}
