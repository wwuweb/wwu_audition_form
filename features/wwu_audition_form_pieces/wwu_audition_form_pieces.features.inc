<?php
/**
 * @file
 * wwu_audition_form_pieces.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_audition_form_pieces_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wwu_audition_form_pieces_node_info() {
  $items = array(
    'audition_timeslot' => array(
      'name' => t('Audition Timeslot'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
