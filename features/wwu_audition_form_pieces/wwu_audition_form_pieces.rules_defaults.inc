<?php
/**
 * @file
 * wwu_audition_form_pieces.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wwu_audition_form_pieces_default_rules_configuration() {
  $items = array();
  $items['rules_audition_sign_up_without_nid'] = entity_import('rules_config', '{ "rules_audition_sign_up_without_nid" : {
      "LABEL" : "Audition Sign-Up without NID",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "webform_rules", "wwu_audition_form", "rules" ],
      "ON" : { "webform_rules_submit" : [] },
      "IF" : [
        { "data_nid" : {
            "webform" : [ "data" ],
            "nid" : { "value" : { "webform-client-form-7" : "webform-client-form-7" } }
          }
        }
      ],
      "DO" : [
        { "concatenate" : {
            "USING" : {
              "string_1" : [ "data:date:0" ],
              "string_2" : [ "data:time:0" ],
              "space" : "1"
            },
            "PROVIDE" : { "final_string" : { "date_string" : "Date String" } }
          }
        },
        { "strtotime" : {
            "USING" : { "input_text" : [ "date-string" ] },
            "PROVIDE" : { "timestamp" : { "converted_time" : "Converted Time" } }
          }
        },
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_timestamp",
              "value" : [ "converted-time" ]
            },
            "PROVIDE" : { "entity_fetched" : { "selected_audition_time" : "Selected Audition Time" } }
          }
        },
        { "variable_add" : {
            "USING" : { "type" : "integer", "value" : [ "data:sid" ] },
            "PROVIDE" : { "variable_added" : { "submission_id" : "Submission ID" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "selected-audition-time" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "component_rules_reserve_audition_for_given_instrument" : {
                  "selected_audition" : [ "list-item" ],
                  "slected_instrument" : [ "data:instrument:0" ],
                  "submission_url" : "node\\/[node:nid]\\/submission\\/[submission-id:value]"
                }
              }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_reserve_audition_for_given_instrument'] = entity_import('rules_config', '{ "rules_reserve_audition_for_given_instrument" : {
      "LABEL" : "Reserve Audition for given instrument",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "selected_audition" : { "label" : "Selected Audition", "type" : "node" },
        "slected_instrument" : { "label" : "Selected Instrument", "type" : "text" },
        "submission_url" : { "label" : "Submission URL", "type" : "text" }
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "selected_audition" ],
            "type" : "node",
            "bundle" : { "value" : { "audition_timeslot" : "audition_timeslot" } }
          }
        },
        { "list_contains" : {
            "list" : [ "selected-audition:field-instrument" ],
            "item" : [ "slected-instrument" ]
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "selected-audition:field-free" ], "value" : "0" } },
        { "mail" : {
            "to" : [ "selected-audition:field-faculty-email" ],
            "subject" : "An audition has been scheduled",
            "message" : "A [selected-audition:field_instrument] audition on [selected-audition:field-timestamp] has been scheduled. Details can be viewed at [site:url][submission-url:value]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
